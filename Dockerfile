FROM node:18.15

WORKDIR ./

COPY . ./

ENV PORT=3001

ENV BACKEND_URL="https://backend.morningnews.ygherve.online"

RUN yarn install --production --ignore-scripts

RUN yarn build

EXPOSE 3001

CMD ["yarn","start"]

